import { IsNotEmpty, MinLength, Matches } from 'class-validator';
export class CreateUserDto {
  @IsNotEmpty()
  @MinLength(5)
  login: string;

  @IsNotEmpty()
  @MinLength(5)
  name: string;

  @MinLength(5)
  @Matches(/@(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/)
  @IsNotEmpty()
  password: string;
}
